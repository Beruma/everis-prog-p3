package dam1.prog.p2;
/*
 * Se pretende realizar un programa que simule el juego del tres en raya, para eso utilizaremos una estructura que
 * permita simular un tablero de 3x3. Hay que cumplir las siguientes reglas para considerar una partida correcta.
 * El objetivo es colocar tres fichas en linea vertical, horzontal, o diagonla. En una primera fase van colocando las
 * fichas en el tablero. Si no se consigue el objetivo se vaciará el tablero y se dará la opción de empezar de nuevo
 * o de salir. Los símbolos a utilizar serán 'X' y 'O'.
 * Existen dos jugadores, y cada uno coloca una ficha en su turno, primero uno. luego el otro, así hasta completar las
 * posibilidades que ofrece el tablero. No se pueden pisar las posiciones ocupadas, con lo que si en una posición está
 * ocupada habrá que pedir al usuario que introduzca otra posición diferente.
 */

import java.util.Scanner;

/**
 * @author Beatriz
 */
public class TresEnRaya {
    public static void main(String[] args) {

        Scanner key = new Scanner(System.in);
        char[][] tablero = new char[3][3];
        char j1 = 'X';
        char j2 = 'O';
        char vacio = '-';
        int fila;
        int columna;
        boolean turno = true;
        boolean posicionOK;

        rellenarTablero(tablero, vacio);

        while (!finPartida(tablero, vacio)) {

            mostrarTurno(turno);
            mostrarTablero(tablero);
            System.out.println();

            do {
                posicionOK = false;
                System.out.println("Dame la fila");
                fila = key.nextInt() - 1;
                System.out.println("Dame la columna");
                columna = key.nextInt() - 1;

                if (validarPosicion(fila, columna)) {
                    if (tablero[fila][columna] == vacio) {
                        if (turno) {
                            ponerFicha(tablero, fila, columna, j1);
                        } else {
                            ponerFicha(tablero, fila, columna, j2);
                        }
                        posicionOK = true;
                    } else {
                        System.out.println("La posición esta ocupada");
                    }
                } else {
                    System.out.println("La posición no es correcta");
                }

            } while (!posicionOK);

            turno = !turno;
        }
        mostrarTablero(tablero);
        mostrarGanador(tablero, j1, j2, vacio);
    }

    public static void rellenarTablero(char[][] tablero, char vacio) {
        for (int fila = 0; fila < tablero.length; fila++) {
            for (int columna = 0; columna < tablero[fila].length; columna++) {
                tablero[fila][columna] = vacio;
            }
        }
    }

    public static boolean validarPosicion(int fila, int columna) {

        return (fila >= 0) && (fila <= 2) && (columna >= 0) && (columna <= 2);
    }

    public static void ponerFicha(char[][] tablero, int fila, int columna, char ficha) {
        tablero[fila][columna] = ficha;
    }

    public static void mostrarTablero(char[][] tablero) {
        for (int fila = 0; fila < tablero.length; fila++) {
            System.out.println();
            for (int columna = 0; columna < tablero[fila].length; columna++) {
                System.out.print(tablero[fila][columna] + " ");
            }
        }
    }

    public static void mostrarTurno(boolean turno) {
        if (turno) {
            System.out.println("Le toca al jugador 1");
        } else {
            System.out.println("Le toca al jugador 2");
        }
    }

    public static char ganadorFila(char[][] tablero, char vacio) {

        char ficha;
        for (int fila = 0; fila < tablero.length; fila++) {
            for (int columna = 0; columna < tablero[fila].length; columna++) {
                if (tablero[fila][0] != vacio) {
                    ficha = tablero[fila][0];
                    if ((ficha == tablero[fila][0])
                            && (ficha == tablero[fila][1])
                            && (ficha == tablero[fila][2])) {
                        return ficha;
                    }
                }
            }
        }
        return vacio;
    }

    public static char ganadorColumna(char[][] tablero, char vacio) {
        char ficha;
        for (int fila = 0; fila < tablero.length; fila++) {
            for (int columna = 0; columna < tablero[fila].length; columna++) {
                if (tablero[0][columna] != vacio) {
                    ficha = tablero[0][columna];
                    if ((ficha == tablero[0][columna])
                            && (ficha == tablero[1][columna])
                            && (ficha == tablero[2][columna])) {
                        return ficha;
                    }
                }
            }
        }
        return vacio;
    }

    public static char ganadorDiagonal(char[][] tablero, char vacio) {
        char ficha;
        for (int fila = 0; fila < tablero.length; fila++) {
            for (int columna = 0; columna < tablero[fila].length; columna++) {
                if (tablero[0][0] != vacio) {
                    ficha = tablero[0][0];
                    if ((ficha == tablero[1][1]) && (ficha == tablero[2][2])) {
                        return ficha;
                    }
                }
            }
        }
        return vacio;
    }

    public static char ganadorDiagonalInversa(char[][] tablero, char vacio) {
        char ficha;
        for (int fila = 0; fila < tablero.length; fila++) {
            for (int columna = 0; columna < tablero[fila].length; columna++) {
                if (tablero[0][0] != vacio) {
                    ficha = tablero[0][2];
                    if ((ficha == tablero[2][2]) && (ficha == tablero[2][0])) {
                        return ficha;
                    }
                }
            }
        }
        return vacio;
    }

    public static boolean tableroLleno(char[][] tablero, char vacio) {
        for (int fila = 0; fila < tablero.length; fila++) {
            for (int columna = 0; columna < tablero[fila].length; columna++) {
                if (tablero[fila][columna] == vacio) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void mostrarGanador(char[][] tablero, char j1, char j2, char vacio) {

        char ficha = ganadorFila(tablero, vacio);
        if (ficha != vacio) {
            if (ficha == j1) {
                System.out.println("Ha ganado el jugador 1 con una fila");
            } else {
                System.out.println("Ha ganado el jugador 2 con una fila");
            }
        }

        ficha = ganadorColumna(tablero, vacio);
        if (ficha != vacio) {
            if (ficha == j1) {
                System.out.println("Ha ganado el jugador 1 con una columna");
            } else {
                System.out.println("Ha ganado el jugador 2 con una columna");
            }
        }

        ficha = ganadorDiagonal(tablero, vacio);
        if (ficha != vacio) {
            if (ficha == j1) {
                System.out.println("Ha ganado el jugador 1 con una diagonal");
            } else {
                System.out.println("Ha ganado el jugador 2 con una diagonal");
            }
        }

        ficha = ganadorDiagonalInversa(tablero, vacio);
        if (ficha != vacio) {
            if (ficha == j1) {
                System.out.println("Ha ganado el jugador 1 con una diagonal");
            } else {
                System.out.println("Ha ganado el jugador 2 con una diagonal");
            }
        }

        if (tableroLleno(tablero, vacio)) {
            System.out.println("Empate");
        }
    }

    public static boolean finPartida(char[][] tablero, char vacio) {
        if ((ganadorFila(tablero, vacio) != vacio)
                || (ganadorColumna(tablero, vacio) != vacio)
                || (ganadorDiagonal(tablero, vacio) != vacio)
                || (ganadorDiagonalInversa(tablero, vacio) != vacio)
                || tableroLleno(tablero, vacio)) {
            return true;
        }
        return false;
    }

}